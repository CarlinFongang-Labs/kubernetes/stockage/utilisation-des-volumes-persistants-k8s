# Lab : Utilisation des volumes persistants dans Kubernetes



------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Intro

Nous allons faire une démonstration pratique de l'utilisation des volumes persistants dans Kubernetes. Nous avons déjà abordé tous les concepts relatifs aux volumes persistants dans la vidéo précédente. Si vous ne l'avez pas encore regardée, nous vous recommandons de le faire pour bien comprendre ce qui se passe dans cette vidéo. Cette vidéo sera une démonstration rapide des concepts que nous avons déjà discutés.

## Création d'une classe de stockage

Nous sommes connectés à notre serveur de plan de contrôle Kubernetes, et nous allons commencer par créer une classe de stockage. Nos volumes persistants et nos requêtes de volumes persistants vont référencer cette classe de stockage, c'est donc le premier objet que nous devons créer ici. Nous allons coller un YAML basique :

Une fois connecté au Control Plan

```bash
nano localdisk-sc.yml
```

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: localdisc
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true
```

Nous appelons notre classe de stockage `localdisc` avec le provisionneur `kubernetes.io/no-provisioner`, et `allowVolumeExpansion` est défini à `true`, ce qui nous permettra d'étendre notre requête de volume persistant plus tard. Vous n'êtes pas obligé de créer une classe de stockage. Si vous définissez simplement un nom de classe de stockage dans un volume persistant, une classe sera automatiquement créée, mais elle n'aura pas `allowVolumeExpansion` défini à `true`. Nous voulons que ce soit le cas, donc nous allons créer manuellement la classe de stockage.

Nous avons créé notre fichier YAML. Maintenant, nous allons utiliser la commande `kubectl create` pour créer notre classe de stockage appelée `localdisc` :

```sh
kubectl create -f localdisc-storageclass.yml
```

>![Alt text](img/image.png)
*Classe de stockage crée*

## Création d'un volume persistant

Maintenant que nous avons notre classe de stockage, nous pouvons créer notre volume persistant. Nous allons créer un fichier YAML appelé `my-pv.yml` et y coller le contenu suivant :

```bash
nano my-pv.yml
```

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  storageClassName: localdisc
  persistentVolumeReclaimPolicy: Recycle
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/var/output"
```

Dans ce fichier, nous définissons un volume persistant appelé `my-pv` avec une classe de stockage `localdisc`, une politique de récupération `Recycle`, une capacité de stockage de `1Gi` et des modes d'accès `ReadWriteOnce`. Le volume persistant utilise un `hostPath`, donc les données seront stockées directement sur l'hôte où le pod est situé, dans le répertoire `/var/output`.

Nous allons sauvegarder et quitter ce fichier, puis créer notre volume persistant avec la commande :

```sh
kubectl create -f my-pv.yml
```

>![Alt text](img/image-1.png)
*PV crée*

Ensuite, nous allons vérifier l'état de notre volume persistant avec la commande `kubectl get pv`. Nous devrions voir notre volume persistant avec le statut `Available` :

```sh
kubectl get pv
```

>![Alt text](img/image-2.png)
*Volume persistant disponible*

## Création d'une requête de volume persistant

Maintenant, nous allons créer une requête de volume persistant. Nous allons créer un fichier YAML appelé `my-pvc.yml` et y coller le contenu suivant :

```bash
nano my-pvc.yml
```

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: localdisc
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
```

Dans ce fichier, nous définissons une requête de volume persistant avec la même classe de stockage `localdisc`, les mêmes modes d'accès `ReadWriteOnce` et une demande de `100Mi` de stockage. Cette requête devrait pouvoir se lier sans problème à notre volume persistant.

Nous allons sauvegarder et quitter ce fichier, puis créer notre requête de volume persistant avec la commande :

```sh
kubectl create -f my-pvc.yml
```

>![Alt text](img/image-3.png)

Ensuite, nous allons vérifier l'état de notre requête de volume persistant avec la commande `kubectl get pvc`. Nous devrions voir que notre requête est liée à notre volume persistant :

```sh
kubectl get pvc
```

>![Alt text](img/image-4.png)
*PVC crée*

Nous allons également vérifier à nouveau l'état de notre volume persistant pour confirmer qu'il est bien lié à la requête de volume persistant :

```sh
kubectl get pv
```

>![Alt text](img/image-5.png)
*PV lié au PVC*

## Création d'un pod utilisant la requête de volume persistant

Maintenant que notre requête de volume persistant est liée à notre volume persistant, nous pouvons créer un pod qui utilise cette requête de volume persistant pour stocker des données. Nous allons créer un fichier YAML appelé `pv-pod.yml` et y coller le contenu suivant :

```bash
nano pv-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pv-pod
spec:
  restartPolicy: Never
  containers:
  - name: my-container
    image: busybox
    command: ['sh', '-c', 'echo "Success" > /output/success.txt']
    volumeMounts:
    - name: pv-storage
      mountPath: /output
  volumes:
  - name: pv-storage
    persistentVolumeClaim:
      claimName: my-pvc
```

Ce pod utilise un conteneur `busybox` qui écrit le message "Success" dans `/output/success.txt`. Le volume est monté à `/output` et fait référence à la requête de volume persistant `my-pvc`.

Nous allons sauvegarder et quitter ce fichier, puis créer notre pod avec la commande :

```sh
kubectl create -f pv-pod.yml
```

>![Alt text](img/image-6.png)
*Création du pod réussie*

Nous allons vérifier que le pod s'est bien exécuté avec la commande `kubectl get pods`. Nous devrions voir que le statut du pod est `Completed` :

```sh
kubectl get pods
```

>![Alt text](img/image-7.png)


Si vous le souhaitez, vous pouvez rechercher quel nœud ce pod particulier a utilisé et vérifier les données créées par ce pod sous `/var/output` dans le worker node adéquate.

il suffira de passer la commande `kubectl get po pv-pod -o wide` pour obtenir le bon worker node.


## Redimensionnement de la requête de volume persistant

Enfin, nous allons montrer comment redimensionner la requête de volume persistant. Nous allons utiliser la commande `kubectl edit` pour éditer notre requête de volume persistant et ajouter l'option `--record` pour enregistrer le processus :

```sh
kubectl edit pvc my-pvc --record
```

>![Alt text](img/image-8.png)
*Edition du pvc à chaud réussie*

Nous allons trouver la section `resources.requests.storage` et la changer de `100Mi` à `200Mi`, puis sauvegarder et quitter.

Nous allons vérifier que le redimensionnement a réussi en utilisant à nouveau la commande `kubectl get pvc` :

```sh
kubectl get pvc
```

>![Alt text](img/image-9.png)

Nous devrions voir que notre requête est toujours liée au volume persistant. Ensuite, nous allons supprimer notre pod et notre requête de volume persistant pour démontrer la politique de recyclage :

```sh
kubectl delete pod pv-pod
kubectl delete pvc my-pvc
```

>![Alt text](img/image-10.png)
*Suppression réussie*

Enfin, nous allons vérifier l'état de notre volume persistant avec la commande `kubectl get pv`. Nous devrions voir que le statut est maintenant `Available`, indiquant que le volume persistant a été recyclé et est prêt à être réutilisé :

```sh
kubectl get pv
```

>![Alt text](img/image-11.png)
*PV disponible*

ainsi, en supprimant notre pvc, avec la politique du pv défini à `persistentVolumeReclaimPolicy: Recycle`, le statut est passé à "disponible".

# Résumé

Dans cette leçon, nous avons fait une démonstration pratique de l'utilisation des volumes persistants dans notre cluster Kubernetes. C'est tout pour cette leçon. Nous vous verrons dans la prochaine.


# Reférences

https://kubernetes.io/docs/concepts/storage/persistent-volumes/

https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/